from django.db import models

class Producto(models.Model):
    titulo = models.CharField(max_length=100)
    precio = models.FloatField()
    descripcion = models.TextField()
    imagen = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.titulo
