from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from .models import Producto

# https://docs.djangoproject.com/es/1.9/topics/class-based-views/generic-editing/
class ProductoForm(ModelForm):
    helper = FormHelper()
    helper.add_input(Submit('guardar', 'Guardar', css_class='btn-primary'))

    class Meta:
        model = Producto
        fields = ['titulo', 'precio', 'descripcion', 'imagen']
