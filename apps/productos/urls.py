from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'nuevo/$', CrearProducto.as_view(), name='crear_producto'),
    url(r'(?P<pk>[0-9]+)/$', ModificarProducto.as_view(), name='modificar_producto'),
    url(r'^$', ProductosView.as_view(), name='productos')
]