from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render
from django.views.generic import TemplateView, CreateView, UpdateView
from .models import Producto
from .forms import ProductoForm

class ProductosView(TemplateView):
    template_name = 'productos.html'

    def get(self, request, *args, **kwargs):
        productos = Producto.objects.all()
        return render(request, self.template_name, { 'productos': productos })


class CrearProducto(CreateView):
    form_class = ProductoForm
    template_name = 'crear_producto.html'
    success_url = reverse_lazy('productos')


class ModificarProducto(UpdateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'crear_producto.html'
    success_url = reverse_lazy('productos')
